<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 17/10/2017
 * Time: 23:40
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class SubFamilyRepository extends EntityRepository
{
    public function alphabeticalOrder()
    {
        return $this->createQueryBuilder('sub_family')
            ->orderBy('sub_family.name', 'ASC');
    }
}