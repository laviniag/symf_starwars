<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 15/10/2017
 * Time: 16:38
 */

namespace AppBundle\Service;

use Doctrine\Common\Cache\Cache;
use Knp\Bundle\MarkdownBundle\MarkdownParserInterface;

class MarkdownTransformer
{
    private $markdownParser;
    private $cache;

    public function __construct(MarkdownParserInterface $markdownParser, Cache $cache)
    {
        $this->markdownParser = $markdownParser;
        $this->cache = $cache;
    }

    public function parse($notes)
    {
        $key = md5($notes);
        if ($this->cache->contains($key)) {
            return $this->cache->fetch($key);
        }
        return $this->markdownParser->transformMarkdown($notes);
    }
}