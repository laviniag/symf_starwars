<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 16/10/2017
 * Time: 11:49
 */

namespace AppBundle\Twig;

use AppBundle\Service\MarkdownTransformer;

class MarkdownExtension extends \Twig_Extension
{
    private $markdownTransformer;

    public function __construct(MarkdownTransformer $markdownTransformer)
    {
        $this->markdownTransformer = $markdownTransformer;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('markdownify', array($this, 'parseMarkdown'), [
                'is_safe' => ['html']
            ])
        ];
    }

    /**
     * @param $str
     * @return mixed|string
     */
    public function parseMarkdown($str)
    {
        return $this->markdownTransformer->parse($str);
    }

}