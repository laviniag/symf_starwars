<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 12/10/2017
 * Time: 18:26
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="genus_habitat")
 */
class GenusHabitat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Genus", inversedBy="linkHabitat")
     * @ORM\JoinColumn(name="genus_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $genus;

    /**
     * @ORM\ManyToOne(targetEntity="Habitat", inversedBy="linkGenus")
     * @ORM\JoinColumn(name="habitat_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $habitat;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getGenus()
    {
        return $this->genus;
    }

    /**
     * @param mixed $genus
     * @return GenusHabitat
     */
    public function setGenus($genus)
    {
        $this->genus = $genus;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHabitat()
    {
        return $this->habitat;
    }

    /**
     * @param mixed $habitat
     * @return GenusHabitat
     */
    public function setHabitat($habitat)
    {
        $this->habitat = $habitat;
        return $this;
    }

}