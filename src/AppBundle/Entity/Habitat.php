<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 12/10/2017
 * Time: 18:26
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="habitat")
 */
class Habitat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $depth;

    /**
     * @ORM\ManyToMany(targetEntity="Genus", mappedBy="linkHabitat")
     */
    private $linkGenus;

    public function __construct()
    {
        $this->linkGenus = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * @param mixed $depth
     * @return Habitat
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLinkGenus()
    {
        return $this->linkGenus;
    }

    /**
     * @param mixed $genus
     * @return Habitat
     */
    public function setLinkGenus($genus)
    {
        $this->linkGenus = $genus;
        return $this;
    }

}