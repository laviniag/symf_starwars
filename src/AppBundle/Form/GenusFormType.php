<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 16/10/2017
 * Time: 19:12
 */

namespace AppBundle\Form;

use AppBundle\Entity\Genus;
use AppBundle\Entity\Habitat;
use AppBundle\Entity\SubFamily;
use AppBundle\Repository\SubFamilyRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GenusFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
            ->add('subFamily', EntityType::class, [
                'class' => SubFamily::class,
                'query_builder' => function(SubFamilyRepository $repo) {
                    return $repo->alphabeticalOrder();
                },
                'placeholder' => 'Choose a Sub Family'
            ])
            ->add('speciesCount')
            ->add('funFact')
            ->add('firstDiscoveredAt', DateType::class, [
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
                'html5' => false
            ])
            ->add('isPublished', ChoiceType::class, [
                'choices' => ['Yes' => true, 'No' => false]
            ])
            ->add('linkHabitat', EntityType::class, array(
                'class' => Habitat::class,
                'choice_label' => 'depth',
                'multiple' => true,
                'expanded' => true
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Genus::class,
            'js_validation' => true
        ]);
    }
}