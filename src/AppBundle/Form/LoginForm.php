<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 19/10/2017
 * Time: 17:31
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('_username')
            ->add('_password', PasswordType::class);
    }
}