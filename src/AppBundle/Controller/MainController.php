<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 09/10/2017
 * Time: 23:44
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends Controller
{
    /**
     * @Route("/")
     */
    public function homepageAction()
    {
        return $this->render('main/homepage.html.twig');
    }
}
