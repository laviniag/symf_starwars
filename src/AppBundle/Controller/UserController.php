<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 20/10/2017
 * Time: 22:28
 */

namespace AppBundle\Controller;

use AppBundle\Form\UserRegistrationForm;
use AppBundle\Security\LoginFormAuthenticator;
use Doctrine\ORM\Mapping as Embedded;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends Controller
{
    /**
     * @Route("/register", name="user_register")
     */
    public function registerAction(Request $request)
    {
        $form = $this->createForm(UserRegistrationForm::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Welcome '.$user->getEmail());
            return $this->get('security.authentication.guard_handler')
                ->authenticateUserAndHandleSuccess($user, $request,
                    $this->get(LoginFormAuthenticator::class), 'main');
        }

        return $this->render('user/register.html.twig', [
            'form' => $form->createView()
        ]);
    }
}