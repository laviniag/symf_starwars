<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 16/10/2017
 * Time: 19:15
 */

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Genus;
use AppBundle\Form\GenusFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class GenusAdminController extends Controller
{
    /**
     * @Route("/admin/genus/new", name="admin_genus_new")
     */
    public function newAction(Request $request)
    {
//        $this->denyAccessUnlessGranted('ROLE_MANAGE_GENUS');

//        $form = $this->createFormBuilder()
//            ->add('name')
//            ->add('speciesCount')
//            ->add('funFact')
//            ->getForm();
        $form = $this->createForm(GenusFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $genus = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($genus);
            $em->flush();

            $this->addFlash('success',
                sprintf('Genus created by you: %s!', $this->getUser()->getEmail()));
            return $this->redirectToRoute('genus_list');
        }

        return $this->render('admin/genus/new.html.twig', [
            'genusForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/genus/{id}/edit", name="admin_genus_edit")
     */
    public function editAction(Request $request, Genus $genus)
    {
        $form = $this->createForm(GenusFormType::class, $genus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $genus = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($genus);
            $em->flush();

            $this->addFlash('success', 'Genus updated!');
            return $this->redirectToRoute('genus_list');
        }

        return $this->render('admin/genus/edit.html.twig', [
            'genusForm' => $form->createView()
        ]);
    }
}