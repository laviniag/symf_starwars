<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 07/10/2017
 * Time: 18:02
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Genus;
use AppBundle\Entity\GenusNote;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * @property  container
 */
class GenusController extends Controller
{
    /**
     * @Route("/genus", name="genus_list")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
//        dump($em->getRepository('AppBundle:Genus'));

//        $genuses = $em->getRepository(Genus::class)->findAll();
//        $genuses = $em->getRepository(Genus::class)->findBy(
//            array('isPublished' => true),
//            array('speciesCount' => 'DESC')
//        );
        $genuses = $em->getRepository(Genus::class)->findAllPublishedOrderedBySize();
        if (! $genuses) {
            throw $this->createNotFoundException('No products found !');
        }
        return $this->render('genus/list.html.twig', [
            'genuses' => $genuses
        ]);
    }

    /**
     * @Route("/genus/new")
     */
    public function newAction()
    {
        $genus = new Genus();
        $genus->setName('Octopus' . rand(1, 100));
        $genus->setSubFamily('Octopodinae');
        $genus->setSpeciesCount(rand(100, 99999));

        $note = new GenusNote();
        $note->setUsername('AquaWeaver');
        $note->setUserAvatarFilename('ryan.jpeg');
        $note->setNote('I counted 8 legs... as they wrapped around me');
        $note->setCreatedAt(new \DateTime('-1 month'));
        $note->setGenus($genus);

        $em = $this->getDoctrine()->getManager();
        $em->persist($genus);
        $em->persist($note);
        $em->flush();

        return new Response('<html><body>Genus created!</body></html>');
    }

    /**
     * @Route("/genus/{genusId}", name="genus_show")
     */
    public function showAction($genusId)
    {
//        return new Response('The genus: '.$genusName);
//        $templating = $this->container->get('templating');
//        $html = $templating->render('genus/show.html.twig', array(
//            'name' => $genusName
//        ));
//        return new Response($html);

//        $notes = 'Octopuses can change the color of their body in just *three-tenths* of a second!';
//        $notes = $this->get('markdown.parser')->transform($notes);

        $em = $this->getDoctrine()->getManager();
        $genus = $em->getRepository(Genus::class)->find($genusId);
        if (!$genus) {
            throw $this->createNotFoundException('Genus not found !');
        }

//        $recentNotes = $genus->getNotes()->filter(function(GenusNote $note) {
//            return $note->getCreatedAt() > new \DateTime('-3 months');
//        });

        $markdownTransformer = $this->get('markdown_transformer');
//        $markdownParser = new MarkdownTransformer($markdownTransformer);
        $funFact = $markdownTransformer->parse($genus->getFunFact());

        $recentNotes = $em->getRepository(GenusNote::class)->findAllRecentNotes($genus);
        return $this->render('genus/show.html.twig', array(
            'genus' => $genus,
            'funFact' => $funFact,
            'recentNoteCount' => $recentNotes
        ));
    }

    /**
     * @Route("/genus/{id}/notes", name="genus_show_notes")
     * @Method("GET")
     */
    public function getNotesAction(Genus $genus)
    {
        $notes = array();
        foreach ($genus->getNotes() as $note) {
            $notes[] = [
                'id' => $note->getId(),
                'username' => $note->getUsername(),
                'avatarUri' => '/images/'.$note->getUserAvatarFilename(),
                'note' => $note->getNote(),
                'date' => $note->getCreatedAt()->format('d-M-Y')
            ];
        }
        $data = ['notes' => $notes];
        return new JsonResponse($data);
    }
}