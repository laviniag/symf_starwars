<?php
/**
 * Created by PhpStorm.
 * User: daniele
 * Date: 11/10/2017
 * Time: 18:50
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Genus;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\Fixtures;

class LoadFixtures implements FixtureInterface
{
    public static $indexF = 0;
    public static $indexH = 0;

    public function load(ObjectManager $manager)
    {
//        $genus = new Genus();
//        $genus->setName('Octopus'.rand(1, 100));
//        $genus->setSubFamily('Octopodinae');
//        $genus->setSpeciesCount(rand(100, 9999));
//        $manager->persist($genus);
//        $manager->flush();

        $objects = Fixtures::load(
            __DIR__.'/fixtures.yml',
            $manager,
            ['providers' => [$this]]
        );
    }

    public function family()
    {
        $genera = [
            'SubF-Octopus',
            'SubF-Balaena',
            'SubF-Orcinus',
            'SubF-Hippocampus',
            'SubF-Asterias',
            'SubF-Amphiprion',
            'SubF-Carcharodon',
            'SubF-Aurelia',
            'SubF-Cucumaria',
            'SubF-Balistoides',
            'SubF-Paralithodes',
            'SubF-Chelonia',
            'SubF-Trichechus',
            'SubF-Eumetopias'
        ];
        return $genera[self::$indexF++];
    }

    public function genus()
    {
        $genera = [
            'Octopus',
            'Balaena',
            'Orcinus',
            'Hippocampus',
            'Asterias',
            'Amphiprion',
            'Carcharodon',
            'Aurelia',
            'Cucumaria',
            'Balistoides',
            'Paralithodes',
            'Chelonia',
            'Trichechus',
            'Eumetopias'
        ];
        $key = array_rand($genera);
        return $genera[$key];
    }

    public function habitat()
    {
        $depth = [
            '0 - 500',
            '500 - 1000',
            '1000 - 2000',
            '2000 - 3000',
            '3000 - 5000',
            '5000 - 9000'
        ];
        return $depth[self::$indexH++];
    }
}